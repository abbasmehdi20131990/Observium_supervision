#!/bin/bash
#Install Observium on Ubuntu 20.04 LTS
#https://codepre.com/en/como-instalar-observium-en-ubuntu-20-04-lts./codepre.com/en/como-instalar-observium-en-ubuntu-20-04-lts.html
sudo apt update
sudo apt upgrade -y
sudo groupadd --system observium
sudo useradd -s /sbin/nologin --system -g observium observium
# Install dependency packages
sudo apt install -y libapache2-mod-php7.4 php7.4-cli php7.4-mysql php7.4-mysqli php7.4-gd php7.4-json php-pear snmpd fping mysql-server mysql-client python3-mysqldb rrdtool subversion whois mtr-tiny ipmitool graphviz imagemagick apache2 python3-pymysql python-is-python3
#Download and install Observium on Ubuntu 20.04
mkdir -p /opt/observium 
cd /opt
sudo wget https://www.observium.org/observium-community-latest.tar.gz
sudo tar zxvf observium-community-latest.tar.gz
create database observium;
sudo mysql -s<<EOF
CREATE DATABASE observium CHARACTER SET UTF8 COLLATE UTF8_BIN;
CREATE USER 'observium'@'%' IDENTIFIED BY 'observium';
GRANT ALL PRIVILEGES ON observium.* TO 'observium'@'%';
flush privileges;
exit;
EOF

sudo cp /opt/observium/config.php.default /opt/observium/config.php
sudo echo -e <<EOF>/opt/observium/config.php
?php

## Check https://docs.observium.org/config_options/ for documentation of possible settings

## It's recommended that settings are edited in the web interface at /settings/ on your observium installation.
## Authentication and Database settings must be hardcoded here because they need to work before you can reach the web-based configuration int>

// Database config
// --- This MUST be configured
$config['db_host']      = 'localhost';
$config['db_name']      = 'observium';
$config['db_user']      = 'observium';
$config['db_pass']      = 'observium';

// Base directory
#$config['install_dir'] = "/opt/observium";

// Default snmp version
#$config['snmp']['version'] = "v2c";
// Snmp max repetition for faster requests
#$config['snmp']['max-rep'] = TRUE;
// Default snmp community list to use when adding/discovering
#$config['snmp']['community'] = [ "public" ];

// Authentication Model
#$config['auth_mechanism'] = "mysql";    // default, other options: ldap, http-auth, please see documentation for config help

// Enable alerter
#$config['poller-wrapper']['alerter'] = TRUE;

// Show or not disabled devices on major pages
#$config['web_show_disabled'] = FALSE;

// Set up a default alerter (email to a single address)
#$config['email']['default']        = "user@your-domain";
#$config['email']['from']           = "Observium <observium@your-domain>";

// End config.php
EOF
sudo mkdir /opt/observium/{rrd,logs}
sudo chown -R observium:observium /opt/observium/
sudo chmod -R 775 /opt/observium/
sudo cp /opt/observium/snmpd.conf.example /etc/snmp/snmpd.conf
sudo tee /etc/snmp/snmpd.conf<<EOF
#  Listen for connections on all interfaces (both IPv4 *and* IPv6)
agentAddress udp:161,udp6:[::1]:161

#  Full view access
view   all         included   .1

#  system + hrSystem groups only
view   systemonly  included   .1.3.6.1.2.1.1
view   systemonly  included   .1.3.6.1.2.1.25.1
#configue chain 
com2sec readonly  default         0bservium
#  Default access to full view
rocommunity 0bs3rv1um  default    -V all

#  Default access to basic system info
#rocommunity public  default    -V systemonly

#  System contact and location
syslocation Rack/Room/Building, Street, City, Country [GPSX,Y]
syscontact Contact Person <your@email.address>

#  Disk Monitoring
disk       /     10000
disk       /var  5%
includeAllDisks  10%

# Unacceptable 1-, 5-, and 15-minute load averages
load   12 10 5
# This line allows Observium to detect the host OS if the distro script is installed
extend .1.3.6.1.4.1.2021.7890.1 distro /usr/local/bin/distro

# This lines allows Observium to detect hardware, vendor and serial
extend .1.3.6.1.4.1.2021.7890.2 hardware /bin/cat /sys/devices/virtual/dmi/id/product_name
extend .1.3.6.1.4.1.2021.7890.3 vendor   /bin/cat /sys/devices/virtual/dmi/id/sys_vendor
extend .1.3.6.1.4.1.2021.7890.4 serial   /bin/cat /sys/devices/virtual/dmi/id/product_serial

# This line allows Observium to collect an accurate uptime
extend uptime /bin/cat /proc/uptime

# This line enables Observium's ifAlias description injection
#pass_persist .1.3.6.1.2.1.31.1.1.1.18 /usr/local/bin/ifAlias_persist
EOF
sudo systemctl restart snmpd
touch /etc/apache2/sites-available/observium.conf
sudo ln -s /etc/apache2/sites-available/observium.conf /etc/apache2/sites-enabled/observium.conf
sudo tee /etc/apache2/sites-available/observium.conf<<EOF
<VirtualHost *:80>
ServerAdmin admin@yourdomain.com
DocumentRoot /opt/observium/html/
ServerName your-domain.com
ServerAlias www.your-domain.com
<Directory /opt/observium/html/>
Options FollowSymLinks
AllowOverride All
Order allow,deny
allow from all
</Directory>
ErrorLog /var/log/apache2/your-domain.com-error_log
CustomLog /var/log/apache2/your-domain.com-access_log common
</VirtualHost>
EOF
sudo a2enmod rewrite
sudo a2ensite observium.conf 
sudo systemctl restart apache2.service
sudo apt install -y certbot python3-certbot-apache
sudo certbot -y --apache --agree-tos --redirect --staple-ocsp --email you@googlesyndication.com -d googlesyndication.com
sudo apache2ctl -t
sudo systemctl reload apache2
cd /opt/observium
./discovery.php -u
./adduser.php admin AdminPass 10
./adduser.php godetz password 10
#Access to Observium in Ubuntu
sudo echo " https://your-domain.com either https://server-ip-address "





